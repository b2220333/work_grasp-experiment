#pragma once

#include <thread>
#include <list>
#include <vector>
#include <mutex>
#include <functional>

#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/math/RPY.hpp>
#include <rw/trajectory/Path.hpp>

#include "hardware/robot.hpp"
#include "hardware/gripper.hpp"


/* WARNING: USE AT YOUR OWN RISK! */
    
namespace hardware {
    
    /**
     * @brief Controls the hardware (robot and gripper) by giving it the commands to run.
     *        Currently all commands are blocking and only one gripper and/or robot is supported.
     * 
     */
    class HardwareControl
    {
    public:
        
        enum TaskRequest {
            ROBOT_PTP_Q, // lin in joint space, going to _q_goal
            ROBOT_LIN_T, // lin in Cartesian space, going to _t_goal
            ROBOT_LIN_Q, // lin in Cartesian space, going to _q_goal
            GRIPPER_PTP_Q, // actuating gripper to go to _q_goal
            GRIPPER_GRASP, // actuating gripper with force of _q_goal
            FORCEMODE_ACTIVATE,
            FORCEMODE_DEACTIVATE,
            SAMPLE_ROBOT_T_OFFSET
        };
        
        struct ControlTask {
            ControlTask(){}
            
            ControlTask(const TaskRequest task):_task(task){}
            
            ControlTask(const TaskRequest task, const rw::math::Q &goal):
            _task(task), _q_goal({goal}){}
            
            ControlTask(const TaskRequest task, const std::vector<rw::math::Q> &goal):
            _task(task), _q_goal(goal){}
            
            ControlTask(const TaskRequest task, const rw::math::Q &goal, const double &speed, const double &blend):
            _task(task), _q_goal({goal}), _speed({speed}), _blend({blend}){}
            
            ControlTask(const TaskRequest task, const rw::math::Transform3D<> &goal):
            _task(task),_t_goal(goal){}
            
            ControlTask(const TaskRequest task, const rw::math::Transform3D<> &goal, const double &speed, const double &blend):
            _task(task), _speed({speed}), _blend({blend}), _t_goal(goal){}
            
            ControlTask(const TaskRequest task, const std::vector<rw::math::Q> &goal, const std::vector<double> &speed, const std::vector<double> &blend):
            _task(task), _q_goal(goal), _speed(speed), _blend(blend){}
            
            TaskRequest _task;
            std::vector<rw::math::Q> _q_goal;
            std::vector<double> _speed, _blend;
            rw::math::Transform3D<> _t_goal;
            rw::math::Vector3D<> *_p_offset = NULL;
            rw::math::RPY<> *_rpy_offset = NULL;
        };
        
    protected:
        enum ControlProgress {
            PRE_TASK, EXECUTE_TASK, POST_TASK
        };
        
        enum TaskAccepting {
          NO_TASK, WAIT_FOR_ACCEPT, TASK_ACCEPTED, FREE_TO_MOVE  
        };
        
    public:
        
        
        HardwareControl();
        
        virtual ~HardwareControl();
        

        bool checkHardwareConnections();

        // used to get the robot config. Takes into account if it is moving.
        rw::math::Q getRobotConfig(); // not  const because caros did not implement it with const
        
        bool isGripperOK() const;
        bool isRobotOK() const;
        
        void stopRobot();
        
        bool isBusy() const;
        size_t taskCount() const;
        
        void connnectToGripper(const std::string &name);
        void connnectToRobot(const std::string &name);
        
        void setRobotDefaultSpeed(const double speed);
        
        void setCallbackSampleQ(std::function< void(const rw::math::Q, ControlTask) > func);
        void setCallbackShowQpath(std::function< void(const rw::trajectory::QPath) > func);
        
        hardware::Robot* getRobot(){
            return _robot;
        }

        hardware::Gripper* getGripper(){
            return _gripper;
        }

        void addTask(const ControlTask& task);
        
        void acceptPath();
        void setFreeToMove(const bool movement);
        
        
    protected:
        
        void hardwareControlThread();

        
        // used to control hardware
        bool _shutdownIndicator, _emgStopSignaled;
        rw::math::Q _robotCurrentQ; // do not change this any other place than in the cotnrol loop
        std::list< ControlTask > _control_tasks; // the set of tasks to perform
        std::mutex _control_task_mutex;
        
        // Robot and griper interfaces
        hardware::Robot* _robot;
        hardware::Gripper* _gripper;
                
        double _robot_default_speed; // value between 0 and 100
        
        TaskAccepting _robot_task_acceptance;
        
        std::function< void(const rw::math::Q, ControlTask) > _callback_sampleQ;
        std::function< void(const rw::trajectory::QPath) > _callback_showQpath;

        std::thread _hardwareControlThread; // the thread running the control
        
    };
    
}

