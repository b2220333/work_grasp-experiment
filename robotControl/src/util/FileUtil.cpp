#include "FileUtil.hpp"

#include <stdexcept>

#include <boost/filesystem.hpp>

bool util::FileUtil::testFilename(const std::string &filetype, const std::string &filename){
    bool ret = false;
    if( filename.size() > filetype.size()){
        if(filename.find(filetype, filename.size() - filetype.size()) != std::string::npos){
            ret = true;
        }
    }

    return ret;
}

bool util::FileUtil::isSCAD(const std::string &filename){
    std::string type = ".scad";
    return FileUtil::testFilename(type, filename);
}

bool util::FileUtil::isSTL(const std::string &filename){
    std::string type = ".stl";
    return FileUtil::testFilename(type, filename);
}


std::string util::FileUtil::generateFilename(const std::string &filebeginning, const std::string &fileending, const bool unique){
    std::string tmpFile = filebeginning;

    time_t timenow;
    std::time(&timenow);
    struct tm zero({0,0,0,0,0,0,0,0,0,0,0});
    double t = std::difftime(timenow, std::mktime(&zero));

    tmpFile += std::to_string(t);
    int l = tmpFile.size();
    tmpFile.erase(l-7, std::string::npos);
    tmpFile += fileending;

    // ensure uniqness
    while(unique && boost::filesystem::exists(tmpFile)){
        // remove ending
        l = tmpFile.size();
        tmpFile.erase(l-fileending.size(), std::string::npos);
        // append char // add ending
        tmpFile += "f" + fileending;

    }
    return tmpFile;
}


std::string util::FileUtil::generateTmpFilename(const std::string &fileending, const bool unique){
    // generate temporary filename by appending _tmp, the current time and .scad
    std::string tmpFile = boost::filesystem::current_path().string() + "/";
    tmpFile += "_tmp";

    time_t timenow;
    std::time(&timenow);
    struct tm zero = {0,0,0,0,0,0,0,0,0,0,0};
    double t = std::difftime(timenow, std::mktime(&zero));

    tmpFile += std::to_string(t);
    int l = tmpFile.size();
    tmpFile.erase(l-7, std::string::npos);
    tmpFile += fileending;

    // ensure uniqness
    while(unique && boost::filesystem::exists(tmpFile)){
        // remove ending
        l = tmpFile.size();
        tmpFile.erase(l-fileending.size(), std::string::npos);
        // append char // add ending
        tmpFile += "f" + fileending;

    }
    return tmpFile;
}


void util::FileUtil::removeFiles(const std::vector<std::string> files){
    for(size_t i = 0; i < files.size(); i++){
        if(boost::filesystem::exists(files[i])){
            boost::filesystem::remove(files[i]);
        }
    }
}

void util::FileUtil::findFilesInFolder(const std::string &path, const std::string &filetype, std::vector<std::string> &files){
    boost::filesystem::path p(path);
    files.clear();
        try {
        if (boost::filesystem::is_directory(p)){
            for(boost::filesystem::directory_iterator it(p); it != boost::filesystem::directory_iterator();it++){
                if(it->path().extension() == filetype){
                    std::string spath = it->path().string();
                    files.push_back(spath);
                } else if(boost::filesystem::is_directory(it->path())){
                    std::vector<std::string> tmpfiles;
                    findFilesInFolder(it->path().string(), filetype, tmpfiles);
                    files.insert(files.end(), tmpfiles.begin(), tmpfiles.end());
                }
            }
        } else {
            throw std::runtime_error("Input was not a directory.");
        }
    } catch (const boost::filesystem::filesystem_error& ex){
        throw std::runtime_error(ex.what());
    }
}











