#pragma once

// std lib
#include <string>
#include <ctime>
#include <vector>

// boost libs
#include <boost/filesystem.hpp>



namespace util {

class FileUtil {
public:

    FileUtil(){}

    virtual ~FileUtil(){}

    static std::string generateTmpFilename(const std::string &fileending, const bool unique = true);
 
    static std::string generateFilename(const std::string &filebeginning, const std::string &fileending, const bool unique = true);

    static void removeFiles(const std::vector< std::string > files);

    static bool isSCAD(const std::string &filename);
    static bool isSTL(const std::string &filename);

    static void findFilesInFolder(const std::string &path, const std::string &filetype, std::vector< std::string > &files);

protected:


    static bool testFilename(const std::string &filetype, const std::string &filename);



};
}



