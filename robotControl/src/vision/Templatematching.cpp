#include "Templatematching.hpp"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>

#include <fstream>
#include <stdexcept>


vision::Templatematching::Templatematching():
    _templateMatchingMethod(CV_TM_SQDIFF), _smoothTemplateMatches(true), _imageMatchingMethod(RAW_IMAGE) 
{}

void vision::Templatematching::loadImages ( const std::vector< std::string >& images, 
                                                      const std::vector< std::string >& imgmask, 
                                                      const std::vector< std::string >& transform, const bool greyImg ) {
    if(images.size() != imgmask.size() || images.size() != transform.size()){
        throw std::runtime_error("Did not recive equal amount of images, masks and transforms.");
    }
    
    _templates.clear();
    _templates.resize(images.size());
    
#pragma omp parallel for
    for(size_t i = 0; i < images.size(); i++){
        _templates.at ( i ).greyImage = greyImg;
        _templates.at(i).transform = loadTransform(transform.at(i));
        cv::Mat img_orig = cv::imread(images.at(i));
        convertImage(img_orig);
        cv::Mat img_mask = cv::imread(imgmask.at(i));
        cv::Rect rect = findRectangle(img_mask);
        // there is a flip in the images generated
        //rect.x = img_orig.cols - rect.x - rect.width;
        rect.y = img_orig.rows - rect.y - rect.height; 
        if(greyImg && img_orig.channels() == 3){
            cv::cvtColor(cv::Mat(img_orig, rect), _templates.at(i).templateImg, CV_BGR2GRAY);
        } else {
            _templates.at(i).templateImg = cv::Mat(img_orig, rect).clone();
        }
        _templates.at(i).origImg = images.at(i);
        _templates.at(i).mask = rect;
        _templates.at(i).cog = cv::Point(img_orig.cols/2 - rect.x, img_orig.rows/2 - rect.y);
    }
}


rw::math::Transform3D<> vision::Templatematching::loadTransform ( const std::string file ) const {
    int rows = 0;
    std::ifstream infile;
    infile.open(file);
    std::vector<double> data(16);
    while (! infile.eof())
    {
        std::string line;
        std::getline(infile, line);
        if(line == ""){
            continue;
        }

        int temp_cols = 0;
        std::stringstream stream(line);
        while(! stream.eof()) {
            if(stream.peek() == ' ' || stream.peek() == ','){
                stream.ignore();
                continue;
            }
            if(rows > 3){
                std::cerr << "Error, got to many rows in load Transform3D.";
            }
            if(temp_cols > 3){
                continue;
            }
            stream >> data.at(rows * 4 + temp_cols);
            temp_cols++;
        }
        rows++;
    }

    rw::math::Transform3D<> trans(rw::math::Vector3D<>(data.at(3), data.at(7), data.at(11)), 
                                  rw::math::Rotation3D<>(data.at(0), data.at(1), data.at(2), data.at(4), data.at(5), data.at(6), data.at(8), data.at(9), data.at(10)));
    
    infile.close();

    return trans;
}

void vision::Templatematching::smoothTemplateMatches ( const bool smooth ) {
    _smoothTemplateMatches = smooth;
}

cv::Rect vision::Templatematching::findRectangle ( const cv::Mat imgmask ) const {
    cv::Rect rectangle;
    bool start_found = false;
    // find start
    int r = 0, c = 0;
    for(r = 0; r < imgmask.rows && !start_found; r++){
        for(c = 0; c < imgmask.cols && !start_found; c++){
         const cv::Vec3b &v = imgmask.at<cv::Vec3b>(r,c);
           if(v[0] == 255){
                start_found = true;
                rectangle.x = c-2;
                rectangle.y = r-2;
            }
        }
    }
    if(rectangle.x < 0){
        rectangle.x = 0;
    }
    if(rectangle.y < 0){
        rectangle.y = 0;
    }
    
    for(int c_it = c+1; c_it < imgmask.cols; c_it++){
        const cv::Vec3b &v = imgmask.at<cv::Vec3b>(r,c_it);
        if(v[0] == 0){
            rectangle.width = c_it - rectangle.x + 1;
            break;
        }
    }
    
    for(int r_it = r+1; r_it < imgmask.rows; r_it++){
        const cv::Vec3b &v = imgmask.at<cv::Vec3b>(r_it,c);
        if(v[0] == 0){
            rectangle.height = r_it - rectangle.y + 1;
            break;
        }
    }
        
    return rectangle;
}


int vision::Templatematching::matchTemplate ( const cv::Mat &img, cv::Rect &result, double &score) const {
    if(_templates.empty()){
        throw std::runtime_error("No templates to match with.");
    }
    std::vector<double> match(_templates.size(), 0.0);
    std::vector<cv::Rect> matchPlace(_templates.size());
    
    if(img.type() != _templates.at(0).templateImg.type()) {
        throw std::runtime_error("Template and image type did not match.");
    }
    
    cv::Mat newimg = img.clone();
    convertImage(newimg);
    
    const int match_method = _templateMatchingMethod;
    const bool smoothMatchingResult = _smoothTemplateMatches;
    int failcount = 0;
#pragma omp parallel for shared(failcount)
    for(size_t i = 0; i < _templates.size(); i++){
        try {
            cv::Mat res_img;
            // find match quality
            const int rows = newimg.rows - _templates.at(i).templateImg.rows;
            const int cols = newimg.cols - _templates.at(i).templateImg.cols;
            if(cols <= 0 || rows <= 0){
                throw std::runtime_error("Template did not fit within image.");
            }
            res_img.create( rows, cols, CV_32FC1 );
            cv::matchTemplate(newimg, _templates.at(i).templateImg, res_img, match_method);
            if(smoothMatchingResult){
                cv::GaussianBlur(res_img, res_img, cv::Size(0,0), 1.0, 1.0);
            }
            /// Localizing the best match with minMaxLoc
            double minVal,  maxVal; 
            cv::Point minLoc, maxLoc;        
            cv::minMaxLoc( res_img, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );

            if( match_method  == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED ) { 
                match.at(i) = minVal;
                matchPlace.at(i).x = minLoc.x;
                matchPlace.at(i).y = minLoc.y;
            } else {
                match.at ( i ) = maxVal;
                matchPlace.at(i).x = maxLoc.x;
                matchPlace.at(i).y = maxLoc.y;
            }
            matchPlace.at(i).width = _templates.at(i).templateImg.cols;
            matchPlace.at(i).height = _templates.at(i).templateImg.rows;
        } catch(std::runtime_error &err) {
            failcount++;
            match.at(i) = -1;
            std::cout << "Got error " << err.what() << " during: " << i << " run using " << _templates.at(i).origImg << std::endl;
        } catch (...){
            failcount++;
            match.at(i) = -1;
            std::cout << "Unknown error occured during: " << i << " run using " << _templates.at(i).origImg << std::endl;
        }
    }
    if( failcount > 0 ){
        std::cout << "Failed to match template " << failcount << " times." << std::endl;
    }
    
    
    int best_match = 0;
    
    if( match_method  == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED ) { 
        for(size_t i = 1; i < _templates.size(); i++){
            if((match.at(i) < match.at(best_match) && match.at(i) >= 0.0) || match.at(best_match) < 0.0){
                best_match = i;
            }
        }
    } else { 
        for(size_t i = 1; i < _templates.size(); i++){
            if(match.at(i) > match.at(best_match)){
                best_match = i;
            }
        }
    }
    if(match.at(best_match) < 0.0){
        throw std::runtime_error("Could not find a template to match.");
    }
    score = match.at(best_match);
    result = matchPlace.at(best_match);
    return best_match;
}


void vision::Templatematching::setImageMatchingMethod ( const vision::Templatematching::ImageMatchingMethod method ) {
    _imageMatchingMethod = method;
    // reload images if conversion from other than raw image format is needed.
#pragma omp parallel for
    for(size_t i = 0; i < _templates.size(); i++){
        cv::Mat img_orig = cv::imread(_templates.at(i).origImg);
        convertImage(img_orig);
        if(_templates.at ( i ).greyImage && img_orig.channels() == 3){
            cv::cvtColor(cv::Mat ( img_orig, _templates.at(i).mask ), _templates.at ( i ).templateImg, CV_BGR2GRAY);
        } else {
            _templates.at ( i ).templateImg = cv::Mat ( img_orig, _templates.at(i).mask ).clone();
        }
    }
}

void vision::Templatematching::convertImage ( cv::Mat& img ) const {
    switch(_imageMatchingMethod){
        case RAW_IMAGE:
            break;
        case CANNY_EDGES:
            convertToCannyEdges(img);
            break;
        case SOBEL_EDGES:
            convertToSobelEdges(img);
            break;
        case LAPLACIAN_EDGES:
            convertToLaplacianEdges(img);
            break;
        default:
            throw std::runtime_error("Method to convert to not recognised.");
            break;
    }
        
}

void vision::Templatematching::convertToCannyEdges ( cv::Mat& img ) const {
    if(img.channels() == 3){
        cv::cvtColor(img, img, CV_BGR2GRAY);
    }
    cv::Canny(img, img, _cannyParams._lowerTreshold, _cannyParams._upperTreshold, _cannyParams._apertureSize, _cannyParams._l2gradient);
}

void vision::Templatematching::setCannyOptions ( const vision::Templatematching::CannyOptions& opt ) {
    _cannyParams = opt;
    setImageMatchingMethod(vision::Templatematching::CANNY_EDGES);
}

void vision::Templatematching::setLaplacianOptions ( const vision::Templatematching::LaplaceOptions& opt ) {
    _laplacianParams = opt;
    setImageMatchingMethod(vision::Templatematching::LAPLACIAN_EDGES);
}

void vision::Templatematching::setSobelOptions ( const vision::Templatematching::SobelOptions& opt ) {
    _sobelParams = opt;
    setImageMatchingMethod(vision::Templatematching::SOBEL_EDGES);
}

void vision::Templatematching::convertToLaplacianEdges ( cv::Mat& img ) const {
    if(img.channels() == 3){
        cv::cvtColor(img, img, CV_BGR2GRAY);
    }
    cv::Laplacian(img, img, -1, _laplacianParams._apertureSize);
}

void vision::Templatematching::convertToSobelEdges ( cv::Mat& img ) const {
    if(img.channels() == 3){
        cv::cvtColor(img, img, CV_BGR2GRAY);
    }
    cv::Sobel(img, img, -1, _sobelParams._xorder, _sobelParams._yorder, _sobelParams._apertureSize);
}









