#pragma once

#include <rw/math/Q.hpp>
#include <rw/trajectory.hpp>


#include "collisionchecker.hpp"

// Implement the path pruning algo

// use a object from collision checker and implement these as classes inheriting a base collision checker class
class pathOptimizer {
public:
  pathOptimizer(CollisionChecker &collChecker);
  
  // path pruner
  void prune(rw::trajectory::QPath &path, double epsilon);
  
protected:
  CollisionChecker* _collChecker;
  
};





