#pragma once

#include <cmath>

#include <rw/common.hpp>
#include <rw/math/Q.hpp>
#include <rw/models.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/proximity/CollisionDetector.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>


//    Implement the four test strategies described in the document, create a small
//    program generating random collision free configurations and tests the edges
//    between them.


class CollisionChecker
{
public:
    CollisionChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector::Ptr &detector);
    
    virtual ~CollisionChecker(){}

    // virtual checker, dependent on method
    virtual bool checkCollision(const rw::math::Q &from, const rw::math::Q &to, const double &epsilon) = 0;


protected:
    // common function
    bool checkConfiguration(const rw::math::Q &configuration);


    // data
    rw::models::Device::Ptr _device;
    rw::kinematics::State _state;
    rw::proximity::CollisionDetector::Ptr _detector;


};


class SFTChecker : public CollisionChecker
{
public:

    SFTChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector::Ptr &detector)
        : CollisionChecker(state, device, detector){}

/**
 * @brief configurationDivision_SFT Generates a path to test using Straight Forward Approach
 * @param from
 * @param to
 * @param epsilon
 * @param checklist
 */
    bool checkCollision(const rw::math::Q &from, const rw::math::Q &to, const double &epsilon);
};

class USSChecker : public CollisionChecker
{
public:

    USSChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector::Ptr &detector)
        : CollisionChecker(state, device, detector){}

/**
 * @brief configurationDivision_USS Generates a path to test using Uniform Step Size
 * @param from
 * @param to
 * @param epsilon
 * @param checklist
 */
    bool checkCollision(const rw::math::Q &from, const rw::math::Q &to, const double &epsilon);
};

class BinChecker : public CollisionChecker
{
public:

    BinChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector::Ptr &detector)
        : CollisionChecker(state, device, detector){}


/**
 * @brief configurationDivision_Bin Generates a path to test using Binary
 * @param from
 * @param to
 * @param epsilon
 * @param checklist
 */
    bool checkCollision(const rw::math::Q &from, const rw::math::Q &to, const double &epsilon);
};

class EBinChecker : public CollisionChecker
{
public:

    EBinChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector::Ptr &detector)
        : CollisionChecker(state, device, detector){}


/**
 * @brief configurationDivision_EBin Generates a path to test using Extended Binary
 * @param from
 * @param to
 * @param epsilon
 * @param checklist
 */
    bool checkCollision(const rw::math::Q &from, const rw::math::Q &to, const double &epsilon);
};

