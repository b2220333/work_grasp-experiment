#include "BasicOption.hpp"

widget::vision::BasicOption::BasicOption ( QWidget& widget, ::vision::Templatematching& algo ):
    _widget(widget), _algorithm(algo), _trigger(NULL)
{}

widget::vision::BasicOption::~BasicOption() {
    clearOptionWidget();
}

void widget::vision::BasicOption::clearOptionWidget() {
    while(!_widgetsObjects.empty()){
        delete _widgetsObjects.front();
        _widgetsObjects.pop_front();
    }
    _widget.update();
}

QPushButton* widget::vision::BasicOption::getTriggerButton() {
    return _trigger;
}


