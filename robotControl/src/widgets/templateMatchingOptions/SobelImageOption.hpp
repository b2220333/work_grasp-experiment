#pragma once

#include <QWidget>

#include "vision/Templatematching.hpp"

#include "BasicOption.hpp"

namespace widget {
    namespace vision {
        
        
        
        class SobelImageOption : public BasicOption {
        public:
            
            SobelImageOption(QWidget &widget, ::vision::Templatematching &algo);
            
            virtual ~SobelImageOption();
            
            
            virtual void updateTemplateMatcher();
            
        protected:
            
            void getSobelEdgeOptions(::vision::Templatematching::SobelOptions &options);
                        
            virtual void drawWidget();
                        
            
            
        };
    }
}

