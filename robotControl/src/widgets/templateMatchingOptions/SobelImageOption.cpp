#include "SobelImageOption.hpp"

#include <QSlider>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>


widget::vision::SobelImageOption::SobelImageOption ( QWidget& widget, ::vision::Templatematching& algo ) : BasicOption ( widget, algo ) {
    if(_algorithm.getImageMatchingMethod() != ::vision::Templatematching::RAW_IMAGE) {
        _algorithm.setImageMatchingMethod(::vision::Templatematching::RAW_IMAGE);
    }
    _algorithm.setImageMatchingMethod(::vision::Templatematching::SOBEL_EDGES);
    
    drawWidget();

}

widget::vision::SobelImageOption::~SobelImageOption() {

}

void widget::vision::SobelImageOption::drawWidget() {
    // add layout http://doc.qt.io/qt-4.8/qhboxlayout.html
    QGridLayout *layout = new QGridLayout();
    _widgetsObjects.push_front(layout);

    const int minHeight = 25;
    // Slider for apperature
    QSlider* xorder = new QSlider(Qt::Horizontal);
    _widgetsObjects.push_front(xorder);
    xorder->setRange(0, 6);
    xorder->setObjectName("xorder");
    xorder->setValue(1);
    xorder->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    xorder->setMinimumHeight(minHeight);
    layout->addWidget(xorder,0,1);
    QLabel *lxorder = new QLabel("X-order");
    _widgetsObjects.push_front(lxorder);
    layout->addWidget(lxorder,0,0);

    // Slider for apperature
    QSlider* yorder = new QSlider(Qt::Horizontal);
    _widgetsObjects.push_front(yorder);
    yorder->setRange(0, 6);
    yorder->setObjectName("yorder");
    yorder->setValue(1);
    yorder->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    yorder->setMinimumHeight(minHeight);
    layout->addWidget(yorder,1,1);
    QLabel *lyorder = new QLabel("Y-order");
    _widgetsObjects.push_front(lyorder);
    layout->addWidget(lyorder,1,0);

    // Slider for apperature
    QSlider* aperture = new QSlider(Qt::Horizontal);
    _widgetsObjects.push_front(aperture);
    aperture->setRange(0, 3);
    aperture->setObjectName("apperture");
    aperture->setValue(3);
    aperture->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    aperture->setMinimumHeight(minHeight);
    layout->addWidget(aperture,2,1);
    QLabel *laperture = new QLabel("Apperture");
    _widgetsObjects.push_front(laperture);
    layout->addWidget(laperture,2,0);
    
    // Btn to upload the settings
    _trigger = new QPushButton("Update Sobel Settings");
    _widgetsObjects.push_front(_trigger);
    _trigger->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    _trigger->setMinimumHeight(minHeight);
    layout->addWidget(_trigger,3,0,1,-1);

    // add the layout and update the plugin
    _widget.setLayout(layout);
    _widget.setMinimumHeight(minHeight * 5);
    _widget.setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
}

void widget::vision::SobelImageOption::updateTemplateMatcher() {
    ::vision::Templatematching::SobelOptions opt;
    getSobelEdgeOptions(opt);
    _algorithm.setSobelOptions(opt);
}

void widget::vision::SobelImageOption::getSobelEdgeOptions ( ::vision::Templatematching::SobelOptions& options ) {
    // search for the objects added
    int i = 0;
    for(std::list< QObject* >::iterator it = _widgetsObjects.begin(); it != _widgetsObjects.end(); it++){
        if((*it)->objectName() == "apperture" && dynamic_cast< QSlider* >(*it) != NULL){
            options._apertureSize = dynamic_cast< QSlider* >(*it)->value() * 2.0 + 1.0;
            i++;
        } else if((*it)->objectName() == "xorder" && dynamic_cast< QSlider* >(*it) != NULL){
            options._xorder = dynamic_cast< QSlider* >(*it)->value();
            i++;
        } else if((*it)->objectName() == "yorder" && dynamic_cast< QSlider* >(*it) != NULL){
            options._yorder = dynamic_cast< QSlider* >(*it)->value();
            i++;
        } 
    }
    if(i != 3){
        throw std::runtime_error("Not all options were found.");
    }
    
    if(options._xorder >= options._apertureSize){
        options._xorder = options._apertureSize - 1.0;
        rw::common::Log::log().info() << "The order of the filter may not be greater or equal to the aperture size." << std::endl;
    }
    if(options._yorder >= options._apertureSize){
        options._yorder = options._apertureSize - 1.0;
        rw::common::Log::log().info() << "The order of the filter may not be greater or equal to the aperture size." << std::endl;
    }
    if(options._yorder + options._xorder <= 0){
        throw std::runtime_error("X+Y order must be greater than 0.");
    }
}



