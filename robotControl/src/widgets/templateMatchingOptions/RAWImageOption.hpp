#pragma once

#include <QWidget>

#include "vision/Templatematching.hpp"

#include "BasicOption.hpp"

namespace widget {
    namespace vision {
        
        
        
        class RAWImageOption : public BasicOption {
        public:
            
            RAWImageOption(QWidget &widget, ::vision::Templatematching &algo);
            
            virtual ~RAWImageOption();
            
            
            virtual void updateTemplateMatcher();
            
        protected:
            
            virtual void drawWidget();
                        
            
            
        };
    }
}

