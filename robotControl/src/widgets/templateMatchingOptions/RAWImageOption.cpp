#include "RAWImageOption.hpp"

widget::vision::RAWImageOption::RAWImageOption ( QWidget& widget, ::vision::Templatematching& algo ) : BasicOption ( widget, algo ) {
    _algorithm.setImageMatchingMethod(::vision::Templatematching::RAW_IMAGE);
}

widget::vision::RAWImageOption::~RAWImageOption() {

}

void widget::vision::RAWImageOption::drawWidget() {

}

void widget::vision::RAWImageOption::updateTemplateMatcher() {

}

