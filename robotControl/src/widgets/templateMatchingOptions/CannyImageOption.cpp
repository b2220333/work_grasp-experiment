#include "CannyImageOption.hpp"

#include <QSlider>
#include <QGridLayout>
#include <QCheckBox>
#include <QPushButton>
#include <QLabel>

widget::vision::CannyImageOption::CannyImageOption ( QWidget& widget, ::vision::Templatematching& algo ) : BasicOption ( widget, algo ) {
    if(_algorithm.getImageMatchingMethod() != ::vision::Templatematching::RAW_IMAGE) {
        _algorithm.setImageMatchingMethod(::vision::Templatematching::RAW_IMAGE);
    }
    _algorithm.setImageMatchingMethod(::vision::Templatematching::CANNY_EDGES);
    
    drawWidget();
}

widget::vision::CannyImageOption::~CannyImageOption() {

}

void widget::vision::CannyImageOption::drawWidget() {
    // add layout http://doc.qt.io/qt-4.8/qhboxlayout.html
    QGridLayout *layout = new QGridLayout();
    _widgetsObjects.push_front(layout);

    const int minHeight = 25;
    // Slider for lower threshold
    QSlider* lowert = new QSlider(Qt::Horizontal);
    _widgetsObjects.push_front(lowert);
    lowert->setTickPosition(QSlider::TickPosition::TicksBelow);
    lowert->setTickInterval(40);
    lowert->setRange(0, 200);
    lowert->setObjectName("lowerthreshold");
    lowert->setValue(10);
    lowert->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    lowert->setMinimumHeight(minHeight);
    layout->addWidget(lowert,0,1);
    QLabel *llowert = new QLabel("Lower Threshold");
    _widgetsObjects.push_front(llowert);
    layout->addWidget(llowert,0,0);
    
    
    // Slider for upper threshold
    QSlider* uppert = new QSlider(Qt::Horizontal);
    _widgetsObjects.push_front(uppert);
    uppert->setTickPosition(QSlider::TickPosition::TicksBelow);
    uppert->setTickInterval(40);
    uppert->setRange(0, 600);
    uppert->setObjectName("upperthreshold");
    uppert->setValue(30);
    uppert->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    uppert->setMinimumHeight(minHeight);
    layout->addWidget(uppert,1,1);
    QLabel *luppert = new QLabel("Upper Threshold");
    _widgetsObjects.push_front(luppert);
    layout->addWidget(luppert,1,0);

    // Slider for apperature
    QSlider* aperture = new QSlider(Qt::Horizontal);
    _widgetsObjects.push_front(aperture);
    aperture->setRange(1, 3);
    aperture->setObjectName("apperture");
    aperture->setValue(3);
    aperture->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    aperture->setMinimumHeight(minHeight);
    layout->addWidget(aperture,2,1);
    QLabel *laperture = new QLabel("Apperture");
    _widgetsObjects.push_front(laperture);
    layout->addWidget(laperture,2,0);
    
    // Checkbox for l2 gradient
    QCheckBox* checkbox = new QCheckBox("L2 Gradient");
    _widgetsObjects.push_front(checkbox);
    checkbox->setObjectName("L2 Gradient");
    checkbox->setChecked(false);
    checkbox->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    checkbox->setMinimumHeight(minHeight);
    layout->addWidget(checkbox,3,0,3,1);
    
    // Btn to upload the settings
    _trigger = new QPushButton("Update Canny Settings");
    _widgetsObjects.push_front(_trigger);
    _trigger->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    _trigger->setMinimumHeight(minHeight);
    layout->addWidget(_trigger,4,0,1,-1);

    // add the layout and update the plugin
    _widget.setLayout(layout);
    _widget.setMinimumHeight(minHeight * 6);
    _widget.setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
}

void widget::vision::CannyImageOption::updateTemplateMatcher() {
    ::vision::Templatematching::CannyOptions opt;
    getCannyEdgeOptions(opt);
    _algorithm.setCannyOptions(opt);
}

void widget::vision::CannyImageOption::getCannyEdgeOptions(::vision::Templatematching::CannyOptions &options) {
    // search for the objects added
    int i = 0;
    for(std::list< QObject* >::iterator it = _widgetsObjects.begin(); it != _widgetsObjects.end(); it++){
        if((*it)->objectName() == "lowerthreshold" && dynamic_cast< QSlider* >(*it) != NULL){
            options._lowerTreshold = dynamic_cast< QSlider* >(*it)->value();
            i++;
        } else if((*it)->objectName() == "upperthreshold" && dynamic_cast< QSlider* >(*it) != NULL){
            options._upperTreshold = dynamic_cast< QSlider* >(*it)->value();
            i++;
        } else if((*it)->objectName() == "L2 Gradient" && dynamic_cast< QCheckBox* >(*it) != NULL){
            options._l2gradient = dynamic_cast< QCheckBox* >(*it)->isChecked();
            i++;
        } else if((*it)->objectName() == "apperture" && dynamic_cast< QSlider* >(*it) != NULL){
            options._apertureSize = dynamic_cast< QSlider* >(*it)->value() * 2.0 + 1.0;
            i++;
        }
    }
    if(i != 4){
        throw std::runtime_error("Not all options were found.");
    }
}
