#include "VisionInterface.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <QString>
#include <QFileDialog>
#include <QSlider>
#include <QGridLayout>
#include <QCheckBox>
#include <QPushButton>
#include <QLabel>


#include <stdexcept>

#include <eigen3/Eigen/Core>

#include <rw/common/Log.hpp>

#include "util/FileUtil.hpp"

#include "widgets/templateMatchingOptions/RAWImageOption.hpp"
#include "widgets/templateMatchingOptions/CannyImageOption.hpp"
#include "widgets/templateMatchingOptions/SobelImageOption.hpp"
#include "widgets/templateMatchingOptions/LaplacianImageOption.hpp"


vision::VisionInterface::VisionInterface(QWidget &parent, QWidget &freespace):
    _parent(parent), _widget_options(freespace),
    _templateMatcherUIWidget(NULL),
    _staticMonoCamera( NULL ),
    _timerCamViewUpdate( NULL ),
    _visiualiseOutput(false),
    _objectBoundingSphereRadius(0.1),
    _imgStorageLocation("")
{
    _timerCamViewUpdate = new QTimer ( this );
    _timerCamViewUpdate->setInterval ( 200 );
    
    connect ( _timerCamViewUpdate, SIGNAL ( timeout() ), this, SLOT ( updateCamView() ) );

    if(!_widget_options.isEnabled()){
        _widget_options.setEnabled(true);
    }
}

vision::VisionInterface::~VisionInterface() {
    // disconnect the camera, if connected
    if( _staticMonoCamera != NULL ){
        _staticMonoCamera->quitNow();
        _staticMonoCamera = NULL;
    }
    
    if(_timerCamViewUpdate != NULL){
        _timerCamViewUpdate->stop();
        delete _timerCamViewUpdate;
        _timerCamViewUpdate = NULL;
    }
    
    if(_templateMatcherUIWidget != NULL){
        delete _templateMatcherUIWidget;
        _templateMatcherUIWidget = NULL;
    }
    
    if(_widget_options.isEnabled()){
        _widget_options.setEnabled(false);
    }
}

void vision::VisionInterface::toggle_showCameraView(bool status) {
    if(status && !_timerCamViewUpdate->isActive()){
        _timerCamViewUpdate->start();
    } else if(!status && _timerCamViewUpdate->isActive()) {
        _timerCamViewUpdate->stop();
    }
}

bool vision::VisionInterface::isConnectedToCamera() const {
    return  (_staticMonoCamera != NULL && _staticMonoCamera->isOK());
}

void vision::VisionInterface::updateCamView() {
    cv::Mat image;
    
    // make sure to get a new image.
    size_t cnt = 0;
    while(!_staticMonoCamera->getIdxImage(image)){
        cnt++;
        if(cnt > 1000){
            return;
        }
        usleep(1000);
    }
    
    if(image.empty()){
        return;
    }
    
    if(_visiualiseOutput && !_visionAlgorithm.getTemplates().empty()){
        std::cout << "Add algorithm to visualise output here." << std::endl;
    }
    
    const std::string windowName = "Mono-Camera View";
    cv::namedWindow( windowName, cv::WINDOW_AUTOSIZE );
    cv::imshow( windowName, image );
    cv::waitKey(10);
}


void vision::VisionInterface::reconnectToMonoCamera(const std::string &topic) {
    toggle_showCameraView(false);
    // reconnect to ws and find device in such        
    if(_staticMonoCamera != NULL){
        _staticMonoCamera->quitNow();
        _staticMonoCamera = NULL;
    }        
    
    _staticMonoCamera = rw::common::ownedPtr( new hardware::Monocamera(topic) );
    _staticMonoCamera->start();
}

void vision::VisionInterface::setDetectionType ( const QString &type ) {
    std::string dettype = type.toStdString();
    if(_templateMatcherUIWidget != NULL){
        if(_templateMatcherUIWidget->getTriggerButton() != NULL){
            disconnect ( _templateMatcherUIWidget->getTriggerButton(), SIGNAL ( pressed() ), this, SLOT ( updateTemplateMatcherOptions() ) );
        }
        delete _templateMatcherUIWidget;
        _templateMatcherUIWidget = NULL;
    }
    if(dettype == "RAW Image"){
        _templateMatcherUIWidget = new widget::vision::RAWImageOption(_widget_options, _visionAlgorithm);
    } else if(dettype == "Canny Edges"){
        _templateMatcherUIWidget = new widget::vision::CannyImageOption(_widget_options, _visionAlgorithm);
    } else if(dettype == "Sobel Edges"){
        _templateMatcherUIWidget = new widget::vision::SobelImageOption(_widget_options, _visionAlgorithm);
    } else if(dettype == "Laplacian Edges"){
        _templateMatcherUIWidget = new widget::vision::LaplacianImageOption(_widget_options, _visionAlgorithm);
    } else {
        throw std::runtime_error("No valid detection type selected for the vision algorithm.");
    }
    
    if(_templateMatcherUIWidget->getTriggerButton() != NULL){
        connect ( _templateMatcherUIWidget->getTriggerButton(), SIGNAL ( pressed() ), this, SLOT ( updateTemplateMatcherOptions() ) );
    }
}

void vision::VisionInterface::toggle_showDetectionOutput ( const bool status ) {
    _visiualiseOutput = status;
}

void vision::VisionInterface::loadTemplates() {
    const QString qfolder = QFileDialog::getExistingDirectory(&_parent, tr("Open Directory"), "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    const std::string folder = qfolder.toStdString();
    
    if(folder.empty()){
        return;
    }
    
    std::vector< std::string > img, transform_files, mask, images_in_folder;
    util::FileUtil::findFilesInFolder(folder, ".png", images_in_folder);
    util::FileUtil::findFilesInFolder(folder, ".txt", transform_files);
        
    for(size_t i = 0; i < images_in_folder.size(); i++){
        if(images_in_folder[i].find("_mask.png") != std::string::npos){
            mask.push_back(images_in_folder[i]);
        } else if(images_in_folder[i].find("_depth.png") != std::string::npos){
        } else {
            img.push_back(images_in_folder[i]);
        }
    }
    
    if(img.size() != mask.size() && img.size() != transform_files.size()){
        throw std::runtime_error("Did not find the same amount of img, masks and txt files (" 
                                 + std::to_string(img.size()) + ", " 
                                 + std::to_string(mask.size()) + ", " 
                                 + std::to_string(transform_files.size()) + ").");
    }
    
    std::sort(img.begin(), img.end());
    std::sort(mask.begin(), mask.end());
    std::sort(transform_files.begin(), transform_files.end());

    _visionAlgorithm.loadImages(img, mask, transform_files, true);
    
    rw::common::Log::log().info() << "Loaded " << _visionAlgorithm.getTemplates().size() << " templates from \n\t'" << folder << "'." << std::endl;
}

void vision::VisionInterface::setObjectBoundingRadius ( const double radius ) {
    if(radius < 0.0){
        return;
    }
    _objectBoundingSphereRadius = radius;
}

void vision::VisionInterface::setChannel(cv::Mat &mat, int channel, unsigned char value){
    // make sure have enough channels
    if (channel >= mat.channels()){
        throw std::runtime_error("Channel not allowed.");
    }

    const int cols = mat.cols;
    const int step = mat.channels();
    const int rows = mat.rows;
    for (int y = 0; y < rows; y++) {
        // get pointer to the first byte to be changed in this row
        unsigned char *p_row = mat.ptr(y) + channel; 
        unsigned char *row_end = p_row + cols*step;
        for (; p_row != row_end; p_row += step)
            *p_row = value;
    }
}

void vision::VisionInterface::locateObject ( const rw::math::Transform3D< double >& camTobj, vision::VisionInterface::VisionData& location ) {
    location._objFound = false;
    location._estimated_camTobj = camTobj;
    if(_staticMonoCamera == NULL || !_staticMonoCamera->isOK()){
        throw std::runtime_error("Not connected to the camera.");
    }
    // get the image
    int cnt = 1000;
    while(!_staticMonoCamera->getImage(location._rawImg)){
        --cnt;
        if(cnt < 0){
            throw std::runtime_error("Could not get an updated image.");
        }
        usleep(1000);
    }
    
    cv::Rect interestregion = getInterestRegion(camTobj, location);
    rw::common::Log::log().info() << "Found the crop box to be: " << interestregion << std::endl;

    // make sure the bb does not go out of frame
    fitCropBox(interestregion, location);
    
    // crop the img according to the bb
    location._processedImg = location._rawImg.clone();
    removeStaticBackground(location._processedImg);
    location._processedImg = location._processedImg(interestregion);
    location._cutOutRegion = interestregion;
    if(location._processedImg.empty()){
        throw std::runtime_error("Object out of view.");
    }

    // match the template
    cv::Rect resultingRect;
    const int templateIdx = _visionAlgorithm.matchTemplate(location._processedImg, resultingRect, location._matchScore);
    location._objFound = true;
    rw::common::Log::log().info() << "Matched the template: " << _visionAlgorithm.getTemplates().at(templateIdx).origImg << std::endl;
    // calculate the point of the template in the image by adding the different cropping "levels"
    location._observed_objCoords.x = interestregion.x + resultingRect.x + _visionAlgorithm.getTemplates().at(templateIdx).cog.x;
    location._observed_objCoords.y = interestregion.y + resultingRect.y + _visionAlgorithm.getTemplates().at(templateIdx).cog.y;
    
    // store the template that was matched to as well
    location._templateImg = _visionAlgorithm.getTemplates().at(templateIdx).templateImg;
    location._templateFoundRegion.height = resultingRect.height;
    location._templateFoundRegion.width = resultingRect.width;
    location._templateFoundRegion.x = interestregion.x + resultingRect.x;
    location._templateFoundRegion.y = interestregion.y + resultingRect.y;
    
    // the objects rotation is that of the template matched
    // position is not known because it is only mono vision
    location._observed_camTobj.R() = _visionAlgorithm.getTemplates().at(templateIdx).transform.R();
    
    // convert to specifeid type for visualisation purpose and draw some of the output of the algorithm
    cv::cvtColor(location._rawImg, location._drawedUpon, CV_GRAY2BGR, 3);
    const int drawsize = 3;
    cv::circle(location._drawedUpon, location._observed_objCoords, drawsize, cv::Scalar(255,0,0), drawsize);
    cv::circle(location._drawedUpon, location._gripperTCP, drawsize, cv::Scalar(0,255,0), drawsize);
    cv::rectangle(location._drawedUpon, location._templateFoundRegion, cv::Scalar(0,255,0), drawsize);
    cv::rectangle(location._drawedUpon, location._cutOutRegion, cv::Scalar(255,0,0), drawsize);        

    cv::Mat dc;
    if(_visionAlgorithm.getImageMatchingMethod() != vision::Templatematching::RAW_IMAGE){
        dc = location._templateImg.clone();
        cv::cvtColor(dc, dc, CV_GRAY2BGR, 3);
        setChannel(dc, 0, 0);
        setChannel(dc, 1, 0);
        _visionAlgorithm.convertImage(location._processedImg);
        if(location._processedImg.channels() != 3){
            cv::cvtColor(location._processedImg, location._processedImg, CV_GRAY2BGR, 3);
        }
        cv::Mat tmp(location._processedImg, resultingRect); 
        tmp += dc;

        cv::Mat sup(location._drawedUpon, location._templateFoundRegion);
        sup += dc;
    }
    
    // store the img if path is given
    if(_imgStorageLocation != ""){
        const std::string tmpFilename = util::FileUtil::generateFilename(_imgStorageLocation, "", true);
        cv::imwrite(tmpFilename + "_raw.png", location._rawImg);    
        cv::imwrite(tmpFilename + "_drawn.png", location._drawedUpon);        
        cv::imwrite(tmpFilename + "_processed.png", location._processedImg);        
    }
}

cv::Rect vision::VisionInterface::getInterestRegion ( const rw::math::Transform3D< double >& camTobj, vision::VisionInterface::VisionData& location ) const {
    // use the transform to crop the image based on the backprojected transform and the bounding sphere of the object.
    const Eigen::Matrix3d intrinsics = _staticMonoCamera->getCameraInfo()._K;
    // calculate the points to the rectangular bb
    const double &x = camTobj.P()[0];
    const double &y = camTobj.P()[1];
    const double &z = camTobj.P()[2];
    const double &fx = intrinsics(0,0);
    const double &fy = intrinsics(1,1);
    const double &cx = intrinsics(0,2);
    const double &cy = intrinsics(1,2);
    const double &r = _objectBoundingSphereRadius;
    
    // calculate the grippers location on the original image
    location._gripperTCP = cv::Point2d(fx * x / z + cx, fy * y / z + cy);
    
    // calculate the crop region
    // camera has +z pointing into scene, +y going down (+u) and x going right (+v) // http://wiki.ros.org/image_pipeline/CameraInfo
    const double fb = fy * (y + r) / (z + r) + cy; // furthest bot
    const double cb = fy * (y + r) / (z - r) + cy; // closest bot
    const double ft = fy * (y - r) / (z + r) + cy; // furthest top
    const double ct = fy * (y - r) / (z - r) + cy; // closest top

    const double fl = fx * (x - r) / (z + r) + cx; // furthest left
    const double cl = fx * (x - r) / (z - r) + cx; // closest left
    const double fr = fx * (x + r) / (z + r) + cx; // furthest rigth
    const double cr = fx * (x + r) / (z - r) + cx; // closest rigth

    cv::Rect interestregion;
    interestregion.x = std::min<double>(fl, cl);
    interestregion.y = std::min<double>(ft, ct);
    interestregion.width = std::max<double>(fr, cr) - interestregion.x;
    interestregion.height = std::max<double>(fb, cb) - interestregion.y;

    return interestregion;
}

void vision::VisionInterface::fitCropBox ( cv::Rect& interestregion, const vision::VisionInterface::VisionData& location ) {
    if(interestregion.x >= location._rawImg.cols || interestregion.y >= location._rawImg.rows // completely outside image (below or to the right)
        || interestregion.x + interestregion.width < 0 || interestregion.y + interestregion.height < 0 // completely outside the image (above or to the left)
    ){
        throw std::runtime_error("BB used to crop is out of view.");
    } 
    bool reduced_cropregion = false;
    if(interestregion.x < 0){
        interestregion.width -= interestregion.x;
        interestregion.x = 0;
        reduced_cropregion = true;
    }
    if(interestregion.y < 0){
        interestregion.height -= interestregion.y;
        interestregion.y = 0;
        reduced_cropregion = true;
    }
    if(interestregion.y + interestregion.height >= location._rawImg.rows){
        interestregion.height = location._rawImg.rows - interestregion.y;
        reduced_cropregion = true;
    }
    if(interestregion.x + interestregion.width >= location._rawImg.cols){
        interestregion.width = location._rawImg.cols - interestregion.x;
        reduced_cropregion = true;
    }
    if(reduced_cropregion){
        rw::common::Log::log().info() << "Reduced the size of the crop box. It should be consider to hold the object a different place." << std::endl;
        rw::common::Log::log().info() << "New size: " << interestregion << std::endl;
    }
}

void vision::VisionInterface::connectToCameraInfo(const std::string& topic) {
    _staticMonoCamera->connectToCameraInfo(topic);
}



void vision::VisionInterface::updateTemplateMatcherOptions() {
    if(_templateMatcherUIWidget == NULL){
        return;
    }
    _templateMatcherUIWidget->updateTemplateMatcher();
}

void vision::VisionInterface::setImgStorageLocation() {
    const QString qfolder = QFileDialog::getExistingDirectory(&_parent, tr("Open Directory"), "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(qfolder.toStdString() != ""){
        _imgStorageLocation = qfolder.toStdString() + "/";
    } else {
        _imgStorageLocation = "";
    }
    
    rw::common::Log::log().info() << "Set the location to store the recorded images to: " << _imgStorageLocation << std::endl;
}

void vision::VisionInterface::setTemplateMatchingMethod ( const QString meth ) {
    if(meth == "SQDIFF"){
        _visionAlgorithm.setMatchingMethod(CV_TM_SQDIFF);
    } else if(meth == "SQDIFF_NORMED"){
        _visionAlgorithm.setMatchingMethod(CV_TM_SQDIFF_NORMED);
    } else if(meth == "CCORR"){
        _visionAlgorithm.setMatchingMethod(CV_TM_CCORR);
    } else if(meth == "CCORR_NORMED"){
        _visionAlgorithm.setMatchingMethod(CV_TM_CCORR_NORMED);
    } else if(meth == "CCOEFF"){
        _visionAlgorithm.setMatchingMethod(CV_TM_CCOEFF);
    } else if(meth == "CCOEFF_NORMED"){
        _visionAlgorithm.setMatchingMethod(CV_TM_CCOEFF_NORMED);
    } else {
        throw std::runtime_error("No known methode input.");
    }
}

void vision::VisionInterface::recordStaticBackgroundImage() {
    int cnt = 100;
    while(!_staticMonoCamera->getImage(_static_background_image)){
        --cnt;
        if(cnt < 0){
            throw std::runtime_error("Could not get an updated image.");
        }
        usleep(1000);
    }
    if(_static_background_image.empty()){
        throw std::runtime_error("Empty image found.");
    }
}

void vision::VisionInterface::removeStaticBackground ( cv::Mat& img ) const {
    if(img.empty() || _static_background_image.empty()){
        rw::common::Log::log().info() << "Empty images." << std::endl;
        return;
    }
    if(img.size() != _static_background_image.size()){
    rw::common::Log::log().info() << "Image size missmatch." << std::endl;
        return;
    }
    
    cv::Mat tmp(img.size(), img.type());
    if(img.channels() == 1){
        if(img.depth() == CV_8U){
            cv::MatIterator_<uchar> itout = tmp.begin<uchar>(), endout = tmp.end<uchar>();
            cv::MatConstIterator_<uchar> itm = _static_background_image.begin<uchar>(), endm = _static_background_image.end<uchar>();
            cv::MatConstIterator_<uchar> it;
            for( it = img.begin<uchar>(); it != img.end<uchar>() && itm != endm && itout != endout; ++it, ++itout, ++itm){
                const double dist = static_cast<double>(*it) - static_cast<double>(*itm);
                (*itout) = static_cast<uchar>((dist / 2.0) + 127.5);
            } 
        } else if(img.depth() == CV_16U){
            cv::MatIterator_<ushort> itout = tmp.begin<ushort>(), endout = tmp.end<ushort>();
            cv::MatConstIterator_<ushort> itm = _static_background_image.begin<ushort>(), endm = _static_background_image.end<ushort>();
            cv::MatConstIterator_<ushort> it;
            for( it = img.begin<ushort>(); it != img.end<ushort>() && itm != endm && itout != endout; ++it, ++itout, ++itm){
                const double dist = static_cast<double>(*it) - *itm;
                (*itout) = static_cast<ushort>((dist / 2.0) + 255.5);
            } 
        } else {
            rw::common::Log::log().info() << "Img type not supported (" << img.depth() << ")." << std::endl;
        }
        
    } else if(img.channels() == 3){
        if(img.depth() == CV_8U){
            cv::MatConstIterator_<cv::Vec3b> itm = _static_background_image.begin<cv::Vec3b>(), endm = _static_background_image.end<cv::Vec3b>();
            cv::MatIterator_<cv::Vec3b> itout = tmp.begin<cv::Vec3b>(), endout = tmp.end<cv::Vec3b>();
            cv::MatConstIterator_<cv::Vec3b> it;
            for( it = img.begin<cv::Vec3b>(); it != img.end<cv::Vec3b>() && itm != endm && itout != endout; ++it, ++itout, ++itm){
                for(size_t i = 0; i < 3; i++){
                    const double dist = static_cast<double>((*it)[i]) - (*itm)[i];
                    (*itout)[i] = static_cast<uchar>((dist / 2.0) + 127.5);
                }
            }
        } else if(img.depth() == CV_16U){
            cv::MatConstIterator_<cv::Vec3w> itm = _static_background_image.begin<cv::Vec3w>(), endm = _static_background_image.end<cv::Vec3w>();
            cv::MatIterator_<cv::Vec3w> itout = tmp.begin<cv::Vec3w>(), endout = tmp.end<cv::Vec3w>();
            cv::MatConstIterator_<cv::Vec3w> it;
            for( it = img.begin<cv::Vec3w>(); it != img.end<cv::Vec3w>() && itm != endm && itout != endout; ++it, ++itout, ++itm){
                for(int i = 0; i < 3; i++){
                    const double dist = static_cast<double>((*it)[i]) - (*itm)[i];
                    (*itout)[i] = static_cast<ushort>((dist / 2.0) + 255.5);
                }
            }            
        } else {
            rw::common::Log::log().info() << "Img type not supported (" << img.depth() << ")." << std::endl;
        }
    } else {
        rw::common::Log::log().info() << "Number of channels not supported (" << img.channels() << ")." << std::endl;
    }
    img = tmp;
}

void vision::VisionInterface::activateSmootheningOfTemplateMatches ( const int smooth ) {
    _visionAlgorithm.smoothTemplateMatches(smooth != 0);
}



