#ifndef ROBOTCONTROL_HPP
#define ROBOTCONTROL_HPP


#include <RobWorkStudioConfig.hpp>
#include <rws/RobWorkStudioPlugin.hpp>

#include <RobWorkStudio.hpp>
#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/MetricUtil.hpp>
#include <rw/models/WorkCell.hpp>
#include <rw/models/Device.hpp>
#include <rw/models/SerialDevice.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/kinematics/Frame.hpp>
#include <rw/kinematics/MovableFrame.hpp>
#include <rw/invkin/ClosedFormIKSolverUR.hpp>


#include "ui_robotcontrol.h"

#include <fstream>
#include <thread>
#include <mutex>
#include <vector>
#include <string>

#include <ros/ros.h>
#include <geometry_msgs/Transform.h>

#include "pathplanning/collisionchecker.hpp"
#include "pathplanning/pathoptimizer.hpp"
#include "hardware/HardwareControl.hpp"

#ifdef USE_VISION
#include "VisionInterface.hpp"
#endif

struct ExperimentalSample {
    rw::math::Vector3D<> _pos, _pos_grasped;
    rw::math::RPY<> _angle, _angle_grasped;
    int _result_wanted = -1, _result_gained = -1;

    std::string dataSave() const {
        std::string ret = "";
	if(_result_gained == 3){
	  ret = transToString(_pos, _angle);
	} else {
	  ret = transToString(_pos_grasped, _angle_grasped);
	}
	ret = ret + ", " + std::to_string(_result_gained) + "\n";
        return ret;
    }
    
    std::string dataShow() const {
        std::string ret = transToString(_pos, _angle) + ","
                + std::to_string(_result_wanted) + "\n";

        return ret;
    }
    
    std::string transToString(const rw::math::Vector3D<> &pos, const rw::math::RPY<> &angle) const {
        std::string ret = std::to_string(pos[0]) + "," + std::to_string(pos[1]) + "," + std::to_string(pos[2]) + ","
                + std::to_string(angle[0] * rw::math::Rad2Deg) + "," + std::to_string(angle[1] * rw::math::Rad2Deg) + "," + std::to_string(angle[2] * rw::math::Rad2Deg);

        return ret;
    }

};



class robotControl: public rws::RobWorkStudioPlugin, private Ui::robotControl 
{
Q_OBJECT
Q_INTERFACES( rws::RobWorkStudioPlugin )

#if RWS_USE_QT5
Q_PLUGIN_METADATA(IID "dk.sdu.mip.Robwork.RobWorkStudioPlugin/0.1" FILE "plugin.json")
#endif
public:
    robotControl();
    
    virtual ~robotControl();

    virtual void open(rw::models::WorkCell* workcell);

    virtual void close();

    virtual void initialize();

public slots:
    
    
    /**
     * @brief Handles must of the btns that are not directly connected to the experiment tab
     * 
     * @return void
     */
    void btnPressed();

    void stateChangedListener(const rw::kinematics::State& state);

    // for experiments
    /**
     * @brief Handles btns connected to the experiment.
     * 
     * @return void
     */
    void btnPressed_exp();
    
    void expCount();


    // Emergency all page
    void btnPressed_stopRobot();
    
    // force mode
    void forcemodetoggle();
    void setPayload();
    
    // signals for content of remote control
    void remoteRecieveTransform();
    void remoteTransformAutoMode();
    void remoteUpdateObjectPose();
    void grasp_remoteRecieveTransform();
    
    void btn_robot_accept_path();
    void btn_toggle_robot_free_to_move(bool isfree);

    
    /**
     * @brief Enters the test fo the different connection settings.
     *        Setttings should be in the format 'devName@rosNamespace', where:
     *             - devName = deivce name in the workcell, and
     *             - rosnamespace = the name of the ros node running.
     *        To add other options, please do so in the .ui file.
     *        New entries are added automaticly if connected to the gripper or robot, those additions stay for the course of the program runtime.
     * 
     * @return void
     */
    void settings_defaults();

    void updateGUITabs();
    
    /**
     * @brief Locates the object in the image (Function only works if vision is used).
     */
    void locateObject();
    void moveRobotToCamera();
    
    void updateRobotDefaultSpeed(const double speed);
    
    
private:
    // functions for experiment
    void updateStatus(const QString &text);
    void loadExperiments();
    void executeNextTest();
    void updateTestCount(const int count);
    void saveResults() const;
    
    /**
     * @brief Used to add extra entries to the combobox with default connections.
     *        Do not use this to add entries in the constructor, add them in the .ui file instead.
     * 
     * @param devName device name in the workcell
     * @param rosNamespace namespace of the ros node to connect to.
     * @return void
     */
    void update_settingsDefault_connections(const std::string &devName, const std::string &rosNamespace);
    
    void placeObject(const rw::math::Transform3D<> &t = rw::math::Transform3D<>(rw::math::Vector3D<>(2, 2, 0.075)));

    // moving around    
    /**
     * @brief Lift the gripper.
     * 
     * @param start Starting configuration (used to resolve which Q corresponding to the transforms it is).
     * @param relativeTo The transform to lift relative to.
     * @param liftheight Height of the lift
     * @param relativeToWorld TRUE: lifts along z-axis in world frame; FALSE: lifts along z-axis of _tcpFrame.
     * @return void
     */
    void liftGripper(const rw::math::Q &start, const rw::math::Transform3D<> &relativeTo, const double liftheight = 0.05, const bool relativeToWorld = true);
    
    /**
     * @brief Goes linearly in Q-space from "from" to "to".
     * 
     * @param from ...
     * @param to ...
     * @param speed ...
     * @param blend ...
     * @return void
     */
    void goToConfiguration(const rw::math::Q &from, const rw::math::Q &to, const double speed = 100.0, const double blend = 0.0);
    
    /**
     * @brief Goes to the Q closest to "from" given the transform from base to _tcpFrame corresponding to "T".
     * 
     * @param from The start configuration
     * @param T The transform to go to (baseT_tcpFrame)
     * @param speed ...
     * @param blend ...
     * @return void
     */
    void goToTransform(const rw::math::Q &from, const rw::math::Transform3D<> &T, const double speed = 100.0, const double blend = 0.0);
    // start is used as the refrence to the invkin when checking poses
    /**
     * @brief Goes from "from" to "to" linearly in cartesian space. robot base to _tcpFrame are the transforms given.
     * 
     * @param start Configuration used as reference to "from".
     * @param from The transform to go from.
     * @param to The goal transform to go to.
     * @param speed ...
     * @param blend ...
     * @return void
     */
    void goToTransform_lin(const rw::math::Q &start, const rw::math::Transform3D<> &from, const rw::math::Transform3D<> &to, const double speed = 100.0, const double blend = 0.0);

    /**
     * @brief Finds the Q nearest to the reference Q. Uses norm2 dist.
     * 
     * @param ref The Q to return the closest to.
     * @param from The set of Qs to test
     * @return rw::math::Q The closest Q from the set "from".
     */
    rw::math::Q findNearestQ(const rw::math::Q &ref, const std::vector< rw::math::Q > &from);
    
    void callback_saveQ(const rw::math::Q config, hardware::HardwareControl::ControlTask task);
    
    void callback_remoteRecieveTransform(const geometry_msgs::TransformConstPtr &transform);
    
    void callback_showQpath(const rw::trajectory::QPath path);
    

    bool checkCollisions(const rw::math::Q &q, const bool printCollidingFrames = false);
    
    /**
     * @brief Solves ambigouties due to 360 degrees turns of the robot joints.
     * 
     * @param config the configuration to solve ambigouties for.
     * @return std::vector< rw::math::Q > All the configurations "config"+'the ambigouties'
     */
    std::vector< rw::math::Q > expandQ(const std::vector< rw::math::Q > &config) const ;
    
    // hardware
    hardware::HardwareControl hwcontrol;
    
    
    // force mode
    void activateForceMode();

    // workcell data
    rw::models::WorkCell::Ptr _wc;
    rw::kinematics::State _state;

    // test vars
    bool _testActive;
    std::string _exp_filename;

    int testnumber_;
    std::vector< ExperimentalSample > _data_exp;

    rw::math::Transform3D<> _origo; // the point used for storing the point of the robot
    rw::kinematics::Frame::Ptr _baseFrame, _tcpFrame;
    rw::math::Q _robotOrigo;

    // object pos
    rw::math::Transform3D<> _object;
    rw::kinematics::MovableFrame::Ptr _mobject, _mpreApprObj;
    
    //
    rw::math::QMetric::Ptr _metric;

    rw::models::SerialDevice::Ptr _sdrobot;
    rw::models::Device::Ptr _drobot, _dgripper;
    rw::invkin::ClosedFormIKSolverUR::Ptr _invkin;
    rw::proximity::CollisionDetector::Ptr _collDetector;
    
    int _robot_accepted_path;
    
    // remote control
    bool _pickUpTransform;
    rw::kinematics::Frame::Ptr _f_remote_ref_frame;
    std::string _topicName_remoteTransforms;
    ros::Subscriber _subscription_remoteTransforms;
    rw::math::Transform3D<> _remoteTransform_camTobj;
    
    // node handle for ros
    ros::NodeHandle* _nh; // used for remote transform subscription
    
    // timer for updating gui
     QTimer *_timer_remote, *_timer_gui;

#ifdef USE_VISION
    vision::VisionInterface * _visionInterface;
    
    rw::kinematics::Frame::Ptr _camera;
    
    std::vector< vision::VisionInterface::VisionData > _data_vision;
#endif
};

#endif /*RINGONHOOKPLUGIN_HPP_*/
